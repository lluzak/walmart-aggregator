require "rails_helper"

RSpec.describe WalmartProductUrlParser do
  describe "self#extract_product_identifier" do
    # rubocop:disable LineLength
    let(:product_urls) do
      {
        "http://www.walmart.com/ip/Ematic-9-Dual-Screen-Portable-DVD-Player-with-Dual-DVD-Players-ED929D/28806789" => "28806789",
        "https://www.walmart.com/ip/51397787?findingMethod=wpa&tgtp=2&cmp=-1&relRank=1&pt=hp&adgrp=-1&plmt=1145x345_B-C-OG_TI_7-20_HL-MID-HP&wpa_qs=_lt_J0ALQYXAu0CTqaunWeDEZFU03QqU6B9iOEd_ZsU&bkt=1776976730&pgid=0&itemId=51397787&relUUID=eb1dd7c0-da64-4cf8-8ca2-b3174a42c36e&adUid=deb79549-895e-4da0-bb60-2dc357596488&adiuuid=1ac1fde8-4bbf-4b6e-abc5-b6d144d2e2bd&adpgm=hl&pltfm=desktop" => "51397787",
        "https://www.walmart.com/ip/Acer-Aspire-E5-575-54E8-15.6-Laptop-Windows-10-Home-Intel-Core-i5-6200U-Dual-Core-Processor-6GB-Memory-1TB-Hard-Drive/51708809?action=product_interest&action_type=title&beacon_version=1.0.2&bucket_id=irsbucketdefault&client_guid=6e14db9b-e5b2-4c00-8164-7d957fa8d755&config_id=2&customer_id_enc&findingMethod=p13n&guid=6e14db9b-e5b2-4c00-8164-7d957fa8d755&item_id=51708809&parent_anchor_item_id=51397787&parent_item_id=51397787&placement_id=irs-2-m3&reporter=recommendations&source=new_site&strategy=PWVUB&visitor_id=QDOZOTuE68IlCTk8o4d1h8" => "51708809"
      }
    end

    it "extracts product identifier from url" do
      product_urls.each do |product_url, product_id|
        expect(described_class.extract_product_identifier(product_url)).to eq(product_id)
      end
    end

    describe "when product url is invalid" do
      it "raises ArgumentError" do
        wrong_url = "http://walmart.pl/ip/Ematic/"
        expect { described_class.extract_product_identifier(wrong_url) }.to raise_error(ArgumentError, /Unable to extract/)
      end
    end
  end
end
