require "rails_helper"

RSpec.describe ProductsController, type: :controller do
  render_views
  subject { do_request }

  describe "GET #index" do
    let(:do_request) { get :index }

    it { is_expected.to be_success }
    it { is_expected.to render_template(:index) }
  end

  describe "GET #show" do
    let(:product)    { create(:product) }
    let(:do_request) { get :show, params: { id: product.id } }

    it { is_expected.to be_success }
    it { is_expected.to render_template(:show) }

    it "assigns @product" do
      do_request
      expect(assigns(:product)).to eq(product)
    end
  end

  describe "POST #create" do
    let(:url)        { "http://www.walmart.com/ip/Ematic-9-Dual-Screen-Portable-DVD-Player-with-Dual-DVD-Players-ED929D/28806789" }
    let(:do_request) { post :create, params: { product_url: url } }

    describe "when product url is correct" do
      describe "when product is successfully added" do
        before do
          expect_any_instance_of(Aggregator::Handlers::FetchProduct).to receive(:call)
        end

        it { is_expected.to redirect_to(products_path) }

        it "sets notice message" do
          do_request
          expect(flash[:notice]).to be_present
        end
      end

      describe "when product already exist in library" do
        before do
          expect_any_instance_of(Aggregator::Handlers::FetchProduct).to receive(:call)
            .and_raise(Aggregator::Errors::ProductAlreadyExistError)
        end

        it { is_expected.to be_success }
        it { is_expected.to render_template(:new) }

        it "sets error message" do
          do_request
          expect(flash[:alert]).to be_present
        end
      end
    end

    describe "When product url is incorrect" do
      let(:url) { "wrong_url" }

      it { is_expected.to be_success }
      it { is_expected.to render_template(:new) }

      it "sets error message" do
        do_request
        expect(flash[:alert]).to be_present
      end
    end
  end
end
