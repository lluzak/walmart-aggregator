require "rails_helper"

RSpec.describe Aggregator::Handlers::FetchProduct do
  let(:command) do
    Aggregator::Commands::FetchProductInformation.new(product_id: 28_806_789)
  end

  subject { described_class.new(command) }

  describe "#call" do
    it "stores product" do
      expect { subject.call }.to change { Product.count }.by(1)
    end

    it "stores product's reviews" do
      expect { subject.call }.to change { Review.count }.by(2)
    end

    describe "when product already exist" do
      let!(:product) { create(:product, uuid: 28_806_789) }

      it "raises ProductAlreadyExistError" do
        expect { subject.call }.to raise_error(Aggregator::Errors::ProductAlreadyExistError)
      end
    end
  end
end
