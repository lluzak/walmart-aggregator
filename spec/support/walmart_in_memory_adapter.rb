class Walmart::InMemoryAdapter < Walmart::BaseAdapter
  def initialize(api_token)
    @api_token = api_token
  end

  def product(id)
    raise NotFoundError, "Product not found: #{id}" if id == -1

    {
      "itemId"    => id,
      "name"      => "Ematic 9\" Dual Screen Portable DVD Player with Dual DVD Players (ED929D)",
      "salePrice" => 119.0
    }
  end

  def reviews(_product_id)
    [
      {
        "title"      => "Review Title",
        "reviewer"   => "John Doe",
        "reviewText" => "review text"
      },
      {
        "title"      => "Review Title 2",
        "reviewer"   => "Jane Doe",
        "reviewText" => "review text 2"
      }
    ]
  end
end
