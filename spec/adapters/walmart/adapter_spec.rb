require "rails_helper"

RSpec.describe "Walmart Adapter" do
  shared_examples "walmart adapter" do
    describe "#product" do
      let(:product_id) { 28_806_789 }

      let(:retrieved_product) do
        subject.product(product_id)
      end

      describe "retrieved product" do
        it "contains proper product id" do
          expect(retrieved_product).to include("itemId" => product_id)
        end

        it "contains name" do
          expect(retrieved_product).to include("name")
        end

        it "contains price" do
          expect(retrieved_product).to include("salePrice" => 119.0)
        end
      end

      describe "when product doesn't exist" do
        it "raises NotFoundError" do
          expect { subject.product(-1) }.to raise_error(Walmart::BaseAdapter::NotFoundError)
        end
      end
    end

    describe "#reviews" do
      let(:product_id) { 28_806_789 }

      it "retrieves product reviews" do
        reviews = subject.reviews(product_id)

        expect(reviews).to be_kind_of(Array)
      end

      describe "retrieved reviews" do
        it "contains title, reviewText, reviewer" do
          review = subject.reviews(product_id).first

          expect(review).to include("title", "reviewText", "reviewer")
        end
      end
    end
  end

  let(:api_token) { ENV["WALMART_API_TOKEN"] }

  describe "when using in-memory adapter" do
    subject { Walmart::InMemoryAdapter.new(api_token) }

    it_behaves_like "walmart adapter"
  end

  describe "When using http adapter", http: true do
    subject { Walmart::HttpAdapter.new(api_token) }

    it_behaves_like "walmart adapter"
  end
end
