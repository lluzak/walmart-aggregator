FactoryGirl.define do
  factory :product do
    name { |i| "Product Name #{i}" }
    uuid 1200
    price 126.5
  end
end
