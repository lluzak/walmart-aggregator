class ProductsController < ApplicationController
  def index
    @products = Aggregator::Queries::Product.new.all_with_reviews
  end

  def show
    @product = Aggregator::Queries::Product.new.find(params[:id])
  end

  def create
    product_id = WalmartProductUrlParser.extract_product_identifier(params[:product_url])
    command    = Aggregator::Commands::FetchProductInformation.new(product_id: product_id)

    Aggregator::Handlers::FetchProduct.new(command).call
    redirect_to products_path, notice: "Successfully added new product."
  rescue ArgumentError, Aggregator::Errors::ProductAlreadyExistError => e
    flash.alert = e.message
    render :new
  end
end
