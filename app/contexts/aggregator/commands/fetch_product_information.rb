class Aggregator::Commands::FetchProductInformation < Dry::Struct
  attribute :product_id, Types::Int
end
