class Aggregator::Repositories::Walmart
  def initialize(adapter = Rails.configuration.aggregator.walmart_adapter)
    @adapter = adapter
  end

  def product(id)
    product = @adapter.product(id)

    {
      id:    product["itemId"],
      name:  product["name"],
      price: product["salePrice"]
    }
  end

  def reviews(product_id)
    reviews = @adapter.reviews(product_id)

    reviews.map do |review|
      {
        title:    review["title"],
        reviewer: review["reviewer"],
        content:  review["reviewText"]
      }
    end
  end
end
