class Aggregator::Repositories::Review
  def create_for(product_id, reviews)
    reviews.each do |review_attributes|
      ::Review.create!(
        product_id: product_id,
        title:      review_attributes[:title],
        reviewer:   review_attributes[:reviewer],
        content:    review_attributes[:content]
      )
    end
  end
end
