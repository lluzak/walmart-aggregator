class Aggregator::Repositories::Product
  def create!(walmart_product_attributes = {})
    ::Product.create!(
      uuid:  walmart_product_attributes[:id],
      name:  walmart_product_attributes[:name],
      price: walmart_product_attributes[:price]
    )
  end

  def exists?(product_uuid)
    ::Product.where(uuid: product_uuid).exists?
  end
end
