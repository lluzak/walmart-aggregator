class Aggregator::Queries::Product
  def find(id)
    ::Product.includes(:reviews).find(id)
  end

  def all_with_reviews
    ::Product.includes(:reviews).all
  end
end
