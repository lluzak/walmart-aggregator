module Aggregator
  class Handlers::FetchProduct
    def initialize(command)
      @command = command
    end

    def call
      raise Errors::ProductAlreadyExistError, "Product already exist in library" if product_repository.exists?(@command.product_id)

      product_attributes = walmart_repository.product(@command.product_id)
      reviews_attributes = walmart_repository.reviews(@command.product_id)

      ActiveRecord::Base.transaction do
        product = product_repository.create!(product_attributes)
        review_repository.create_for(product.id, reviews_attributes)
      end
    end

    private

    def product_repository
      Repositories::Product.new
    end

    def review_repository
      Repositories::Review.new
    end

    def walmart_repository
      Repositories::Walmart.new
    end
  end
end
