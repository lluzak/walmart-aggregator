class Walmart::BaseAdapter
  BaseError     = Class.new(StandardError)
  NotFoundError = Class.new(BaseError)

  private

  def not_found?(response)
    [404, 400].include?(response.status)
  end
end
