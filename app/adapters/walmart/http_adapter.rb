class Walmart::HttpAdapter < Walmart::BaseAdapter
  def initialize(api_token)
    @api_token = api_token
  end

  def product(id)
    response = client.get build_path("/v1/items/#{id}")

    raise NotFoundError, "Product not found: #{id}" if not_found?(response)
    JSON.parse(response.body)
  end

  def reviews(product_id)
    response = client.get build_path("/v1/reviews/#{product_id}")

    JSON.parse(response.body)["reviews"]
  end

  private

  def build_path(path, params = {})
    default_params = { apiKey: @api_token, format: :json }
    params = default_params.merge(params)

    [path, "?", params.to_query].join
  end

  def client
    @client ||= Faraday.new(url: "http://api.walmartlabs.com/") do |faraday|
      faraday.request  :url_encoded
      faraday.response :logger
      faraday.adapter  Faraday.default_adapter
    end
  end
end
