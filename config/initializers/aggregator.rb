Rails.configuration.aggregator = OpenStruct.new(
  walmart_adapter: Walmart::HttpAdapter.new(ENV["WALMART_API_TOKEN"])
)
