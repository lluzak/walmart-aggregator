class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.integer :uuid, null: false
      t.string  :name, null: false
      t.decimal :price, null: false

      t.timestamps
    end

    add_index :products, :uuid, unique: true
  end
end
