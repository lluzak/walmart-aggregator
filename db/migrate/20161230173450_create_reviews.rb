class CreateReviews < ActiveRecord::Migration[5.0]
  def change
    create_table :reviews do |t|
      t.string  :reviewer,   null: false
      t.string  :title,      null: false
      t.text    :content,    null: false
      t.integer :product_id, null: false

      t.timestamps
    end

    add_foreign_key :reviews, :products
  end
end
