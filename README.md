# Walmart Aggregator

Allows to add products from Walmart.com using url.

### Testing
```
bundle exec rspec
bundle exec rspec --tag http (requires Walmart API token)
bundle exec rubocop
```

### Features
* allow to add new product by using Walmart Product URL ( 3 formats support)
* allow to look though all added products
* allow to see a specific product reviews
* prevent product duplication

### Todo

* filter the reviews by a given keyword ( i would use Postgresql Full-Text feature)
* handle Walmart API errors like ```Timeout```,  ```NotFound``` etc
* handle Walmart API limitations (5 calls per second)
* move product retrieve operation to background job
* allow to update reviews, which even with Walmart API is hard to accomplish, because there isn't ```ReviewId``` returned. Probably by using hash function on ```reviewer``` and ```submissionTime``` attributes could give unique identifier, but that would need to be investigated.
