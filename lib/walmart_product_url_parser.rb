class WalmartProductUrlParser
  URL_TYPES = [
    %r{walmart\.com/ip/[0-9A-z\-\.]+/(?<id>[0-9]+)\??},
    %r{walmart\.com/ip/(?<id>[0-9]+)\?}
  ].freeze

  def self.extract_product_identifier(url)
    URL_TYPES.each do |url_type|
      result = url.match(url_type)
      return result[:id] if result
    end

    raise ArgumentError, "Unable to extract product identifier from url"
  end
end
